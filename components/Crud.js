import React,{useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from 'react-native';
import {useDispatch,useSelector} from 'redux-thunk';


const Crud = (props) => {
    const data = useSelector((state) => state.counter);
    const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <Text style={styles.name}>Enter Name</Text>
      <TextInput style={styles.input} />
      <TouchableOpacity style={styles.btn} onPress={()=>dispatch(add())}>
        <Text style={styles.btt}>Add</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.btn} onPress={()=>dispatch(remove())}>
        <Text style={styles.btt}>Delete</Text>
      </TouchableOpacity>

      <Text style={styles.output}>{data} </Text>
      <TextInput style={styles.input}></TextInput>
    </View>
  );
};

export default Crud;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  btn: {
    backgroundColor: 'blue',
    borderWidth: 1,
    width: 50,
    marginLeft: '40%',
  },
  btt: {
    fontSize: 20,
    color: '#fff',
  },
  name: {
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
  },
  output: {
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    marginTop: 30,
  },
});
