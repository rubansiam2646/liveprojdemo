
const styles = StyleSheet.create({
    welcome: {
        marginTop: 60,
        marginLeft: 40,
        fontFamily:"Roboto-MediumItalic",
        
      },
      t_1: {
        fontSize: 40,
        fontWeight: 'bold',
        letterSpacing: 1,
        color: '#200561',
      },
      t_2: {
        fontSize: 20,
        letterSpacing: 1,
        color: '#200561',
        opacity: 0.5,
      },
      forms: {
        justifyContent: 'center',
        width: '70%',
        marginTop: 70,
        marginLeft: 40,
      },
      input_1: {
        fontSize: 20,
        borderBottomWidth: 1,
      },
      input_2: {
        fontSize: 20,
        borderBottomWidth: 1,
        marginTop: 40,
      },
      b1: {
        fontSize: 25,
        backgroundColor: 'blue',
        width: '60%',
        textAlign: 'center',
        marginLeft: 65,
        borderWidth: 1,
        borderRadius: 5,
        padding: 5,
        color: 'white',
        fontWeight: '600',
        letterSpacing: 1,
      },
      c_3: {
        marginTop: 70,
      },
      media: {
        textAlign: 'center',
        marginTop: 10,
        color: '#200561',
        fontSize: 20,
      },
      // icons:{
      //     flex: 1,
      //     flexDirection:'row',
      //     justifyContent:'center',
      //     marginTop:10,
    
      // },
      fb: {
        height: 30,
        width: 30,
        marginRight: 15,
      },
      ins: {
        height: 30,
        width: 30,
      },
      wp: {
        height: 30,
        width: 30,
        marginLeft: 15,
      },
      last: {
        marginTop: 30,
        textAlign: 'center',
        color: '#200561',
        fontSize: 15,
      },
});