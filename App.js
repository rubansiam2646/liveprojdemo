import React from 'react'
import { View, Text } from 'react-native'
import Intro from './components/Intro'
import Login from './components/Login'
import Crud from './components/Crud'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


const Stack = createNativeStackNavigator();


const App = () => {
   
    return (
        <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
        <Stack.Screen name="Intro" component={Intro}  options={{headerShown: false}} />
        <Stack.Screen name="Crud" component={Crud}  options={{headerShown: false}} />
      </Stack.Navigator>
    </NavigationContainer>
    )
}

export default App
